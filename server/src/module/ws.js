const WebSocket = require('ws')
const MongoDB = require('./mongodb.js')
const db = new MongoDB()
const moment = require('moment')

let clientsArr = []

module.exports = {
  getOnlineClients: function () {
    return clientsArr
  },
  link: function (port) {
    const wss = new WebSocket.Server({
      port: port
    })

    console.log(`ws.js-->WebSocketServer😄 监听： ws://localhost:${port}`)

    /** 设置定时器，定时ping服务器 */
    const isAliveInterval = setInterval(() => {
      // 每次检查的时候，把最新的在线列表推给全局
      clientsArr = Array.from(wss.clients)
      wss.clients.forEach((client) => {
        // 当该客户端已经断开连接则关闭该连接
        if (client.isAlive === false) return client.terminate()
        // 重置所有客户端存活状态为false
        client.isAlive = false
        // ping客户端，马上就会收到客户端发回的pong，如果没则客户端断开连接
        client.ping()
      })
      // 15秒检测一次客户端是否在线
    }, 15000)

    /** 连接关闭时激活，一般情况下不会关闭wss */
    wss.on('close', () => {
      console.log('ws.js-->wss->close')
      clearInterval(isAliveInterval)
    })

    wss.on('connection', (ws) => {
      // 每当一个客户端新建连接时，激活connection，传入ws对象就是该客户端对象
      // 挂上isAlive标记客户端在线状态
      ws.isAlive = true
      ws.on('pong', () => {
        // 当收到客户端ping时, 立即回复pong。如果没收到pong则ws.isAlive=false，**秒后被关闭
        ws.isAlive = true
      })

      ws.on('message', (message) => {
        // 收到客户端消息时激活
        const msgObj = JSON.parse(message)
        console.log('ws.js-->msgObj.from、msgObj.to😄', msgObj.from, msgObj.to)
        /** wss.clients为 set集合，转换为数组便于操作 */
        clientsArr = Array.from(wss.clients)
        // 对于type的不用，执行不同的操作
        // 集合不同的函数
        const route = {
          // 点对点聊天
          chat,
          // 客户端上线
          online,
          // 客户端退出登录
          exit,
          // 清除未读标记
          clearUnReadMsg,
          // 添加好友
          addFriend,
          // 处理好友请求
          addFriendReply,
          inputStatus
        }
        const routeArr = []
        console.log('ws.js-->msgObj.type->', msgObj.type)
        Object.keys(route).forEach((value) => routeArr.push(value))
        // 根据msgObj.type定位route里的方法并调用
        if (routeArr.includes(msgObj.type)) {
          console.log(
            'ws.js-->routeArr.includes(msgObj.type):',
            routeArr.includes(msgObj.type)
          )
          return route[msgObj.type](msgObj, wss, ws)
        }
      })
    })
  }
}

/**
 * @api {WebSocket}
 * @apiName 1
 * @apiVersion 1.0.0
 * @apiGroup WebSocket
 * @apiSampleRequest off
 *
 * @apiExample 请求示例:
 * {
 *   // 消息发送人
 *   "from": "test@163.com",
 *   // 类型
 *   "type": "online"
 * }
 */
// 客户端上线
function online(msgObj, wss, ws) {
  let userIDStatus = false
  wss.clients.forEach((client) => {
    if (client.userID === msgObj.from) {
      userIDStatus = true
    }
  })
  console.log('online:userIDStatus->', userIDStatus)
  /** userIDStatus为false则用户没有重复在线 */
  if (!userIDStatus) {
    ws.userID = msgObj.from
    clientsArr = Array.from(wss.clients)
  }
}

// 客户端退出登录
function exit(msgObj, wss, ws) {
  wss.clients.forEach((client, key) => {
    if (client.userID === msgObj.from) {
      ws.send(
        JSON.stringify({
          error: false,
          msg: '退出登录',
          type: 'exit'
        })
      )
      client.userID = ''
      client.isAlive = false
      // 从在线客户端列表中删除客户端
      clientsArr.splice(key, 1)
    }
  })
}

// 点对点聊天
async function chat(MsgObj, wss) {
  // 调用当前堆栈
  // console.trace()
  // 查询条件
  const myQuery = {
    userID: MsgObj.from,
    chatObj: MsgObj.to
  }
  const youQuery = {
    userID: MsgObj.to,
    chatObj: MsgObj.from
  }
  // 插入数据
  const myChat = {
    msg: MsgObj.msg.content,
    time: MsgObj.msg.time,
    say: 'me',
    msgID: MsgObj.msgID,
    size: MsgObj.size ? MsgObj.size : '',
    postfix: MsgObj.postfix ? MsgObj.postfix : '',
    rawName: MsgObj.rawName ? MsgObj.rawName : '',
    type: MsgObj.file ? 'file' : 'chat',
    status: MsgObj.status
  }
  const youChat = {
    time: MsgObj.msg.time,
    msg: MsgObj.msg.content,
    say: 'you',
    msgID: MsgObj.msgID,
    size: MsgObj.size ? MsgObj.size : '',
    postfix: MsgObj.postfix ? MsgObj.postfix : '',
    rawName: MsgObj.rawName ? MsgObj.rawName : '',
    type: MsgObj.file ? 'file' : 'chat',
    status: MsgObj.status
  }
  // 调用数据库插入聊天记录
  db.updateOne(
    'chatRecord',
    myQuery,
    {
      $push: { chat: myChat },
      $set: { currentChatDate: MsgObj.msg.time }
    },
    { upsert: true }
  ).then()
  db.updateOne(
    'chatRecord',
    youQuery,
    {
      $push: { chat: youChat },
      $set: { currentChatDate: MsgObj.msg.time }
    },
    { upsert: true }
  ).then()
  await db.updateMany(
    'chatRecord',
    {
      $or: [
        {
          userID: MsgObj.from,
          chatObj: MsgObj.to
        },
        {
          userID: MsgObj.to,
          chatObj: MsgObj.from
        }
      ]
    },
    {
      $inc: { count: +1 }
    },
    {
      upsert: true
    }
  )
  // 消息是否发出，如果未发出则保存到未读消息表
  let unRead = true
  console.log('ws.js -> function: chat', MsgObj, '\n<-----')
  wss.clients.forEach((client) => {
    if (client.userID === MsgObj.to) {
      client.send(JSON.stringify(MsgObj))
      unRead = false
    }
  })
  if (unRead) {
    // 当消息是未读状态的时候，更新未读消息条数
    db.updateOne(
      'unReadMessage',
      {
        from: MsgObj.from,
        to: MsgObj.to
      },
      {
        $inc: { count: +1 }
      },
      {
        upsert: true
      }
    ).then()
  }
}

// 清除未读标记
async function clearUnReadMsg(msgObj) {
  db.updateOne(
    'unReadMessage',
    {
      from: msgObj.from,
      to: msgObj.to
    },
    {
      $set: { count: 0 }
    },
    {
      upsert: true
    }
  ).then()
}

// 添加好友
async function addFriend(msgObj, wss, ws) {
  const friApplyResult = await db.query('friendApply', {
    from: msgObj.from,
    to: msgObj.to
  })
  if (friApplyResult.length > 0) {
    // 重复发送请求，不理他
    ws.send(
      JSON.stringify({
        error: true,
        message: '请勿重复添加',
        type: 'addFriend'
      })
    )
  } else {
    // 进入发送请求的流程,type由friendApply变成handleApply
    msgObj.type = 'handleApply'
    const queryMatch = {
      $match: {
        email: msgObj.from
      }
    }
    const queryProject = {
      $project: {
        _id: 0,
        pwd: 0,
        access: 0
      }
    }
    // 查找用户信息
    const userResult = await db.aggregate('user', [queryMatch, queryProject])
    msgObj.fromInfo = userResult[0]
    let unRead = true // 消息是否发出，如果未发出则保存到未读消息表
    wss.clients.forEach((client) => {
      if (client.userID === msgObj.to) {
        client.send(JSON.stringify(msgObj))
        unRead = false
      }
    })
    if (unRead) {
      // 调用数据库添加数据
      await db.insertOneData('friendApply', msgObj)
    }
    ws.send(
      JSON.stringify({
        error: false,
        message: '请求已发送',
        type: 'addFriend'
      })
    )
  }
}

// 处理好友请求
async function addFriendReply(msgObj, wss) {
  let Msg
  const time = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
  /** 同意好友申请的操作 true */
  if (msgObj.status) {
    /** 写入好友关系 */
    db.insertManyData('friend', [
      {
        UID: msgObj.from,
        Friend: msgObj.to,
        time
      },
      {
        UID: msgObj.to,
        Friend: msgObj.from,
        time
      }
    ]).then()
    /** 数据库好友申请表状态改为status: true */
    db.updateOne(
      'friendApply',
      {
        from: msgObj.from,
        to: msgObj.to
      },
      {
        $set: {
          status: true
        }
      },
      {}
    ).then()
    await db.insertManyData('chatRecord', [
      {
        userID: msgObj.from,
        chatObj: msgObj.to,
        currentChatDate: time,
        count: 1,
        chat: [
          {
            time: time,
            msg: '已通过好友申请',
            say: 'you',
            msgID: msgObj.from + Date.now() + msgObj.to,
            postfix: '',
            rawName: '',
            type: 'chat',
            status: true
          }
        ]
      },
      {
        userID: msgObj.to,
        chatObj: msgObj.from,
        currentChatDate: time,
        count: 1,
        chat: [
          {
            time: time,
            msg: '已通过好友申请',
            say: 'you',
            msgID: msgObj.from + Date.now() + msgObj.to,
            postfix: '',
            rawName: '',
            type: 'chat',
            status: true
          }
        ]
      }
    ])

    Msg = {
      from: msgObj.from,
      to: msgObj.to,
      error: false,
      message: '已通过好友申请',
      type: 'reload',
      time
    }
  } else {
    /** 好友请求拒绝 */
    Msg = {
      from: msgObj.from,
      to: msgObj.to,
      error: true,
      message: '好友请求未通过',
      type: 'addFriendReply',
      time
    }
    // 删除好友请求表中的记录
    db.deleteOneData('friendApply', {
      from: msgObj.from,
      to: msgObj.to
    }).then()
  }
  wss.clients.forEach((client) => {
    if (client.userID === msgObj.from || client.userID === msgObj.to)
      client.send(JSON.stringify(Msg))
  })
}

/**
 * @api {WebSocket} 聊天面板输入状态
 * @apiName 7
 * @apiVersion 1.0.0
 * @apiGroup WebSocket
 * @apiSampleRequest off
 *
 * @apiExample 请求示例:
 * {
 *   "from": "test@163.com", // 消息发送人
 *   "to": "test@163.com",        // 接收人
 *   "inputStatus": false,             // 输入状态
 *   "type": "inputStatus"        // 类型
 * }
 */
async function inputStatus(msgObj, wss) {
  wss.clients.forEach((client) => {
    if (client.userID === msgObj.to) {
      console.log('ws.js-->inputStatus:msgObj、client', msgObj, client)
      client.send(JSON.stringify(msgObj))
    }
  })
}
