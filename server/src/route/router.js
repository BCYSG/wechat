const router = require('@koa/router')()
// 引入接口方法
const commonRouter = require('./commonRouter')
const routeAdmin = require('./admin')
const routeLogin = require('./login')
const routeUpload = require('./upload')

router
  .use(routeLogin)
  .use(commonRouter)
  .use(routeUpload)
  .use(routeAdmin)

module.exports = router
