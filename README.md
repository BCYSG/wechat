<h1 align="center">WeCat</h1>

<p align="center">
    <a href="https://gitee.com/BCYSG/wechat">
        <img src="https://img.shields.io/badge/项目地址-gitee.com/BCYSG/wechat-green.svg?style=flat-square" alt="项目地址">
    </a>
    <a href="https://gitee.com/BCYSG/wechat">
        <img src="https://img.shields.io/badge/主页-gitee.com/BCYSG-blue.svg?style=flat-square" alt="博客地址">
    </a>
</p>

### 介绍

* 基于Vue.js全家桶 + WebSocket + Node.js + Koa2 + MongoDB的即时聊天项目
* 功能部分参考微信。
* 项目前后端和部署通过学习，查看他人项目完成 <https://gitee.com/imzusheng/Wecat>。

### 项目展示

>#####   1. 登录

![登录](./项目展示/登录.png)

>#####   2. 注册

![注册](./项目展示/注册.png)

>#####   3. 找回密码

![找回密码](./项目展示/找回密码.png)

>#####   4. 注册成功，选择头像

![注册成功](./项目展示/注册成功.png)

>#####   5. 主面板

![主面板](./项目展示/主界面.png)

>#####   6. 添加好友

![添加好友](./项目展示/添加好友.png)

>#####   7. 好友信息面板

![好友信息面板](./项目展示/好友信息面板.png)

>#####   8. 发送信息和文件

![发送信息和文件](./项目展示/发送信息文件.png)

>#####   9. 多功能面板，发送表情

![多功能面板](./项目展示/多功能面板发送表情.png)

>#####   10. 个性化设置菜单

![个性化设置菜单](./项目展示/设置菜单.png)

>#####   11. 管理员页，用户信息

![管理员页](./项目展示/管理员页面.png)

>#####   12. 查询所有聊天记录

![查询所有聊天记录](./项目展示/所有聊天记录.png)

>#####   13. 聊天记录详情

![聊天记录详情](./项目展示/聊天记录.png)

### 目录结构

>   1. server 是后端服务器
>   2. public 是静态文件
>   3. db 是MongoDB导出备份
>   4. src 是前端项目文件夹

### 安装教程

>   1. 克隆项目```git clone https://gitee.com/BCYSG/wechat.git```
>   2. cd 项目根目录，安装依赖 ```npm install```
>   3. 运行服务 ```npm run serve```
>   4. cd ./server，再安装服务器依赖 ```npm install```
>   5. 导入数据库 ```mongorestore -d wecat ./db```
>   6. 运行服务器 ```npm run dev```

### 测试账号

> userID：**test@163.com**  
> password：**a123456**

### TODO
- [x] 登录
- [x] 防止重复登录
- [x] 最近登录时间
- [x] 最近登录地点（省-市-区）
- [x] 注册（邮箱验证）
- [x] 找回密码（邮箱验证）
- [x] 选择用户头像
- [x] 登出
- [x] 好友点对点聊天
- [x] 好友上线和下线提示
- [x] 加好友及验证好友请求
- [x] 好友请求通知
- [x] 未读消息提示
- [x] 搜索用户
- [x] 用户个性化配置
- [x] 聊天记录懒加载
- [x] 网站标题实时显示未读消息条数
- [x] 最近聊天的好友置顶
- [x] 后台管理
- [x] 聊天图片与文件上传（分片上传）
- [x] 聊天图片与文件预览和下载
- [ ] 群聊
- [x] 用户资料卡
- [ ] 删除好友
- [ ] 创建群
- [ ] 群资料卡
- [ ] 加群
- [ ] 退群

### 相关技术栈

* **VueCli 3**
* **Vuex**
* **Node.js**
* **Element-ui**
* **axios**
* **MongoDB**
* **koa2**
* **WebSocket**
* **ws**

### 关于

* 👉 **邮箱 3039647429@qq.com**
* 👉 [项目地址](https://gitee.com/BCYSG/wechat)  
* 👉 [关于我](https://gitee.com/BCYSG)
* 👉 [github](https://github.com/YICUIN)  
* 👉 [gitee](https://gitee.com/BCYSG)