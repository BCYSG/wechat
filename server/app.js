// 导入 koa
const koaBody = require('koa-body')
const cors = require('koa2-cors')
const Koa = require('koa')

// 实例化 koa
const app = new Koa()
// 路由
const router = require('./src/route/router')
// 引入 验证token
const commonFunction = require('./src/module/commonFunction')
// 获取配置信息
const Config = require('./config')

// WebSocketServer服务器跑起来
require('./src/module/ws').link(Config.wsServerPort)
app
  // 接收文件配置
  .use(koaBody(Config.koaOptions))
  // 处理跨域
  .use(cors())
  // 验证
  .use((ctx, next) => commonFunction.verifyToken(ctx, next))
  // 注册路由使路由生效
  .use(router.routes())
  // 检测服务器所支持的请求方法 allowedMethods，官方文档推荐，自动根据ctx.status设置响应头
  .use(router.allowedMethods())

  // 监听启动端口
app.listen(Config.serverPort, () => {
  console.log(`ENV = ${process.env.NODE_ENV} and 服务器 运行： http://localhost:${Config.serverPort}`)
})
