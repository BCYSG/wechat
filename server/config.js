module.exports = {
  // 'mongodb://wcadmin:123456@localhost:27017/wecheck'
  mongoUrl: 'mongodb://localhost:27017/wecat', // ?authSource=admin
  dbName: 'wecat',
  serverPort: 3800,
  wsServerPort: 4800,
  // 静态资源路径
  staticPath: './upload/',
  // 用koaBody接收文件，koa-multer乱，设置 Koa.js 应用中的文件上传功能
  koaOptions: {
    // 服务器应该解析带有 Content-Type: multipart/form-data 的传入请求
    multipart: true,
    formidable: {
      // uploadDir: Config.staticPath, // 设置文件上传目录
      // name = `${file.name}-${Date.now()}-${file.originalname}`
      // maxFieldsSize: 1000 * 1024 * 1024, // 文件上传大小
      multipart: true,
      // 保持文件的后缀
      keepExtensions: true,
      // 文件上传前的设置
      onFileBegin: (name, file) => {
      }
    }
  }
}
